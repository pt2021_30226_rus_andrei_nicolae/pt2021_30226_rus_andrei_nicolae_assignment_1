package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;


public class View extends JFrame { //clasa pt Frame
    JPanel p1, p2, p3, p4;
    JLabel labelPolinom1, labelPolinom2, labelRezultat; //Label pt input si output
    JTextField polinom1, polinom2; // la fel, doar cu text
    JTextArea rezultat;
    JComboBox choose; // alegerea
    JButton enter; //buton de ENTER

    public View() { //aici le setez, dimensiunile
        //declar si Panelurile
        p1 = new JPanel();
        p1.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));

        p2 = new JPanel();
        p2.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));

        p3 = new JPanel();
        p3.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));

        p4 = new JPanel();

        String[] options = {"adunare", "scadere", "inmultire", "impartire", "derivare", "integrare"}; // ce optiuni am
        choose = new JComboBox(options); // initializez cu optiunile

        enter = new JButton("Enter"); // adaug butonul
        rezultat = new JTextArea(5,20); // textul pt rezultat

        labelRezultat = new JLabel("Rezultat"); //le creez
        labelPolinom1 = new JLabel("model.Polinom 1:");
        labelPolinom2 = new JLabel("model.Polinom 2:");
        polinom1 = new JTextField(20); // text polinoame
        polinom2 = new JTextField(20);


        p1.add(labelPolinom1); //adaug in Panel
        p1.add(polinom1);

        p2.add(labelPolinom2);
        p2.add(polinom2);

        p3.add(choose);
        p3.add(enter);

        p4.add(labelRezultat);
        p4.add(rezultat);

        this.setTitle("Polynomial calculator"); //adaug titlu

        this.add(p1);  //adaug in Frame Panelurile
        this.setContentPane(p1);
        this.add(p2);
        this.add(p3);
        this.add(p4);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //la inchidere se termina programul
        this.setSize(300, 600); // setez dimensiunea
        this.setVisible(true); //o fac vizibila
    }

    public String getPolinom1() {
        return polinom1.getText();
    } // returneaza stringul din Input la polinom 1
    public String getPolinom2() {
        return polinom2.getText();
    } // returneaza stringul din Input la polinom 2
    public void setRezultat(String txt)
    {
        rezultat.setText(txt);
    } // seteaza rezultatul pe output
    public void enterListener(ActionListener l)
    {
        enter.addActionListener(l);
    } // adaug listener la butonul ENTER
    public String getChoice()
    {
        return choose.getSelectedItem().toString();
    } // returneaza selectia prezenta
}







