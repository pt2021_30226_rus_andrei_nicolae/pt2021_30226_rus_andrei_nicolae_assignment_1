import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import model.Model;
import model.Polinom;
import org.junit.*;


public class OperationsTest { //testarea operatiilor

    @Test //testez suma
    public void sumaTest()
    {
        Polinom polinom1 = new Polinom("2x^3-1x^2+5x^1-2x^0 ");  // dau niste polinoame ca stringuri ca input
        Polinom polinom2 = new Polinom("-3x^4-3x^2-35x^1+6x^0 ");
        Model m = new Model(polinom1,polinom2);

        Polinom suma = m.suma(); // fac suma
        assertEquals("-3.0x^4+2.0x^3-4.0x^2-30.0x^1+4.0x^0", suma.getTermeni()); // primul string e rezultatul corect. Daca sunt egale, e succes
    }

    @Test
    public void diferentaTest() // acelasi lucru, dar scadere
    {
        Polinom polinom1 = new Polinom("2x^3-1x^2+5x^1-2x^0 ");
        Polinom polinom2 = new Polinom("-3x^4-3x^2-35x^1+6x^0 ");
        Model m = new Model(polinom1,polinom2);

        Polinom diferenta = m.scadere();
        assertEquals("+3.0x^4+2.0x^3+2.0x^2+40.0x^1-8.0x^0", diferenta.getTermeni()); // primul e rezultatul facut in cap si compar
    }

    @Test
    public void inmultireTest() // la fel
    {
        Polinom polinom1 = new Polinom("3x^3-2x^0 ");
        Polinom polinom2 = new Polinom("1x^2-3x^0 ");
        Model m = new Model(polinom1,polinom2);

        Polinom inmultire = m.inmultire();
        assertEquals("+3.0x^5-9.0x^3-2.0x^2+6.0x^0", inmultire.getTermeni());
    }

    @Test
    public void impartireTest() // la fel, da verific si restul
    {
        Polinom polinom1 = new Polinom("3x^3-1x^2+2x^0 "); // impart polinom1 la polinom2
        Polinom polinom2 = new Polinom("1x^1-3x^0 ");
        Model m = new Model(polinom1,polinom2);

        Polinom impartire = m.impartire();
        Polinom rest = m.getRest(); // fac operatia
        assertTrue("+3.0x^2+8.0x^1+24.0x^0".equals(impartire.getTermeni()) && rest.getTermeni().equals("+74.0x^0") ); // daca rezultatele sunt corecte, e ok
    }

    @Test
    public void derivareTest() // acelasi lucru da am doar un polinom
    {
        Polinom polinom1 = new Polinom("3x^5-5x^2+2x^0 ");
        Model m = new Model(polinom1,new Polinom());

        Polinom derivare = m.derivare();
        assertEquals("+15.0x^4-10.0x^1", derivare.getTermeni());
    }

    @Test
    public void integrareTest() // la fel doar ca pt integrare
    {
        Polinom polinom1 = new Polinom("36x^5-6x^2+10x^1-1x^0 ");
        Model m = new Model(polinom1,new Polinom());

        Polinom integrare = m.integrare();
        assertEquals("+6.0x^6-2.0x^3+5.0x^2-1.0x^1", integrare.getTermeni());
    }

}
