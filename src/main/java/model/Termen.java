package model;

import model.Polinom;

public class Termen extends Polinom { // termen din polinom

    private int ordin;
    private double valoare;

    public Termen(int ordin,double valoare) //initializare
    {
        this.ordin = ordin;
        this.valoare = valoare;
    }
    @Override
    public String toString() // afisarea unui termen prorpiu zis in functie de semn
    {
        int valInt = (int)(valoare*100); // il afisez cu 2 zecimale
        valoare = valInt/100.0;
        if(valoare>0)
            return "+"+valoare+"x^"+ordin;
        if(valoare<0) return valoare+"x^"+ordin;
        else return ""; // nu pun termenul daca e nul
    }
    public String getOrdin()
    {
        return String.valueOf(ordin);
    } // pt a afla ordinul
    public int getOrdinInt(){
        return ordin;
    } // la fel da returneaza int
    public double getValoareInt(){
        return valoare;
    } // returneaza valoarea

    public void setValoare(double valoare)
    {
        this.valoare = valoare;
    } // seteaza o valoare noua
}
