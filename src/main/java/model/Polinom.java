package model;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Polinom { // clasa pt un model.Polinom

    private List<Termen> termeni= new LinkedList(); // lista de termeni a polinomului

    public Polinom(String s)
    {
        this.stringToPolinom(s);
    } // convertirea sirului de caractere primit in polinom
    public Polinom() // initializarea fara nimic
    {

    }
    private void stringToPolinom(String s) // convertire String is model.Polinom si alocare termeni
    {
        boolean putere = false; // ca sa stiu cand cifrele is pt puteri
        String build=""; // sirul construit
        int ordin,valoare=0;
        for (char c : s.toCharArray()) { // parcurg caracterele
            if(c=='^' && !putere) { // daca dau de putere, salvez  numarul de dinainte in valoarea propriu zisa, "valoare"
                valoare = Integer.parseInt(build);
                putere = true; // urmatoarele cifre vor fi puteri
                build=""; // initializez construirea
            }
            else if(putere && Character.isDigit(c)) /* daca inainte am dat de ^ si am dupa cifre, construiesc puterea*/{
                    build += c;
            }
            else if(c=='x' && (build.equals("-") || build.equals("+"))) {// pentru coeficientul 1. Daca am semn inainte si x, coef. e 1
                build+="1";
            }
            else if(c=='+' || c=='-' || Character.isDigit(c) || c==' ') { // daca am semn sau cifra sau spatiu (pentru terminare sir)
                if(putere) {// daca aveam putere inainte
                    ordin = Integer.parseInt(build); //salvez ordinul
                    build=""; //resetez construirea
                    Termen t = new Termen(ordin,valoare); // construiesc termenul
                    termeni.add(t); // il adaug in lista
                    putere = false; // resetez puterea
                }
                build += c; // salvez semnul sau semnul
            }
        }
        this.sortTermeni(); // sortez dupa ordin
    }
    public void sortTermeni() // functia de sortare
    {
        Collections.sort(termeni, new Comparator<Termen>() {

            public int compare(Termen t1, Termen t2) {
                return t2.getOrdin().compareTo(t1.getOrdin()); //compar in functie de ordin
            }
        });
    }

    public String getTermeni() // returnarea termenilor sub forma de String
    {
        String string="";
        for(Termen t : termeni)
        {
            string += t.toString();
        }
        return string;
    }

    public List<Termen> allTermeni()
    {
        return termeni;
    } // returnez lista de termeni
    public void addTermen (Termen t)
    {
        termeni.add(t);
    } // adaugarea unui termen
    public void deleteTermen(Termen t)
    {
        termeni.remove(t);
    } // stergerea unui termen



}
