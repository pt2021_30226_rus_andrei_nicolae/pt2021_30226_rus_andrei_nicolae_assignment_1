package model;

public class Model { // aici sunt operatile
    private Polinom p1; // primul polinom
    private Polinom p2; // si al doilea
    private Polinom rest; // pt impartire, restul

    public Model(String p1,String p2)  // se creeaza polinoamele pe baza stringului
    {
        this.p1 = new Polinom(p1);
        this.p2 = new Polinom(p2);
    }
    public Model(Polinom p1, Polinom p2) // se creeaza polinoamele pe baza a niste polinoame create deja
    {
        this.p1 = p1;
        this.p2 = p2;
    }
    public Polinom suma() //suma
    {
        int valoare=0; // valoarea prezenta
        Polinom suma = new Polinom(); // polinomul returnat - suma
        for(Termen t1 : p1.allTermeni()) // iau fiecare termen din model.Polinom 1
        {
            valoare += t1.getValoareInt(); // adaug valoare din termenul curent
            for(Termen t2 : p2.allTermeni()) // iau fiecare termen din model.Polinom 2
            {
                if(t1.getOrdin().equals(t2.getOrdin())) // daca au acelasi ordin, le adun
                {
                    valoare += t2.getValoareInt();
                    p2.deleteTermen(t2); // il sterg sa nu mai am problema
                    break; // fiecare ordin apare cel mult o data
                }
            }
            Termen termen= new Termen(t1.getOrdinInt(),valoare); // creez termenul in functie de ordin si suma
            valoare=0;
            suma.addTermen(termen); // il adaug
        }
        for(Termen t2 : p2.allTermeni()) // daca au ramas termeni in P2, ii adaug
            suma.addTermen(t2);
        suma.sortTermeni(); //sortez dupa ordin descresc
        return suma;
    }
    public Polinom scadere() // acelasi lucru ca la suma
    {
        double valoare=0;
        Polinom diferenta = new Polinom();
        for(Termen t1 : p1.allTermeni()) // iau termen cu termen
        {
            valoare += t1.getValoareInt();
            for(Termen t2 : p2.allTermeni()) // termen cu termen din p2
            {
                if(t1.getOrdin().equals(t2.getOrdin())) //daca ordinul e comun
                {
                    valoare -= t2.getValoareInt();//scad
                    p2.deleteTermen(t2); //il steerg sa nu se repete
                    break; // ma opresc
                }
            }
            Termen termen= new Termen(t1.getOrdinInt(),valoare); // creez germenul
            valoare=0;
            diferenta.addTermen(termen); // il adaug
        }
        for(Termen t2 : p2.allTermeni()) { // daca raman termeni nerepartizati
            diferenta.addTermen(new Termen(t2.getOrdinInt(),t2.getValoareInt()*(-1))); //il adaug da cu semn schimbat
        }
        diferenta.sortTermeni(); // ii pun in ordine
        return diferenta;
    }
    public Polinom inmultire()
    {
        Polinom produs = new Polinom(); // polinomul din produs
        int ordin;double valoare;
        for(Termen t1 : p1.allTermeni()) //parcurg termenii p1
        {
            for(Termen t2 : p2.allTermeni()) //parcurg termenii p2
            {
                valoare =  t1.getValoareInt() * t2.getValoareInt(); // inmultesc fiecare valoare din p1 cu fiecare valoare din p2
                ordin = t1.getOrdinInt() + t2.getOrdinInt(); // adun ordinele
                Termen termen = new Termen(ordin,valoare); //creez termenul
                produs.addTermen(termen); // adaug in produs termenul creat
            }
        }
        produs = simplificarePolinom(produs); // sfarsit simplific polinomul (daca am mai mult de o valoare de acelasi ordin
        return produs;
    }
    private static Polinom simplificarePolinom(Polinom p)
    {
        Polinom simplificare = new Polinom(); // polinomul simplificat
        for(Termen t1 : p.allTermeni()) //parcurg termenii
        {
            boolean match = false; // se activeaza daca regasim in "simplificare" acelasi ordin cu alt termen din p
            for(Termen t2 : simplificare.allTermeni()) // parcurg termenii din polinomul simplificat creat
            {
                if(t1.getOrdinInt() == t2.getOrdinInt()) // daca cumva a mai fost adugat ordinul respectiv
                {
                    t2.setValoare(t2.getValoareInt()+t1.getValoareInt()); // ii adaug valoarea de ordin comun a lui p la polinomul simplificare
                    match = true; // am gasit acelasi ordin
                    break;
                }
            }
            if(!match) // daca nu s-a gasit dublura
                simplificare.addTermen(t1); // il adaug normal

        }
        return  simplificare;// polinomul simplificat
    }

    public Polinom derivare()
    {
        Polinom deriv = new Polinom(); // valoarea derivarii
        for(Termen t : p1.allTermeni()) // parcurg termenii
        {
            if(t.getOrdinInt()>0) { //daca gradul e peste 0 (la 0 se ignora)
                Termen ter = new Termen(t.getOrdinInt() - 1, t.getValoareInt() * t.getOrdinInt()); // creez termen cu ordin-1 si valoare inmultita cu ordinul
                deriv.addTermen(ter); // il adaug in noul polinom
            }
        }
        return deriv;
    }

    public Polinom integrare()
    {
        Polinom integrare = new Polinom();
        for(Termen t : p1.allTermeni())
        {
            Termen ter = new Termen(t.getOrdinInt() + 1, t.getValoareInt() / (t.getOrdinInt() +1 ));
            integrare.addTermen(ter);
        }
        return integrare;
    }

    public Polinom impartire()
    {
        Polinom catul = new Polinom();
        Polinom dif ; // polinomul inmultit cu termenul ce trebuie scazut (vezi mai jos)
        Polinom copieP1 = p1; // deimpartitul
        while(copieP1.allTermeni().get(0).getOrdinInt() >= p2.allTermeni().get(0).getOrdinInt() )
        {  //cat timp ordinul primului termen a scaderii succesive (din p1) este mai mare sau egal decat ordinul primului termen al impartitorulului, tot impart
            if(copieP1.allTermeni().get(0).getValoareInt()==0) // daca cumva valoarea primului termen din scadere e nula
            {
                copieP1.allTermeni().remove(copieP1.allTermeni().get(0)); // sterg primul termen (de grad maxim)
                continue; // sar la iteratia urmatoare
            }
            int newOrdin = copieP1.allTermeni().get(0).getOrdinInt() - p2.allTermeni().get(0).getOrdinInt(); // scad din ordinul  deimpartitului, impartitorul (PRIMUL TERMEN)
            double newValue = copieP1.allTermeni().get(0).getValoareInt() / p2.allTermeni().get(0).getValoareInt(); // impart valorile deimpartitului la impartitor (primul termen)

            Polinom monom = new Polinom(); // creez un "monom" (polinom cu un singur termen)
            Termen t = new Termen(newOrdin,newValue); // creez termenul din valorile calculate mai sus
            monom.addTermen(t); // il adaug conform algoritmului impartirii

            Model mInm = new Model(p2,monom); // declar un model de Inmultire, ca sa inmultesc acel monom de mai sus, cu impartitorul (p2)
            dif = mInm.inmultire(); // fac inmultirea intre p2 si monomul calculat - acesta va fi elementul ce va fi scazut din deimparitit

            Model mDif = new Model(copieP1,dif); // mai fac un model de unde voi scadea din deimpartit, elementul construit prin inmultire
            copieP1 = mDif.scadere(); // fac scaderea, iar scaderea va fi in copieP1, adica noul deimpartit
            catul.addTermen(t); // adaug in cat termenul care s-a inmultit cu impartitorul

            // repet procedeul pana cand gradul copieP! va fi mai mic decat al lui P2 (impartitor)

        }
        rest = copieP1; // restul va fi ce ramane in deimpartit
        return catul;
    }

    public Polinom getRest() {
        return rest;
    } // returnez restul
}
