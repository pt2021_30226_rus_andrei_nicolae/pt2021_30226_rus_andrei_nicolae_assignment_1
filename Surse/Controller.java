import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller { //controlarea evenimentului

    private View view; // interfata grafica

    public Controller(View view)
    {
        this.view = view;

        view.enterListener(new Happens()); // adaug Listener
    }

    class Happens implements ActionListener{ // clasa ascultatorului

        public void actionPerformed(ActionEvent event) //daca apas butonul
        {
            Model model = new Model(view.getPolinom1(),view.getPolinom2()); //creez polinoamele
            Polinom polinom = new Polinom(); //polinomul de afisat
            Polinom rest = new Polinom(); //restul pt impartire
            String choice = view.getChoice(); //alegerea

            if ("adunare".equals(choice)) { //aici fac operatia in functie de alegere
                polinom = model.suma(); //pun in rezultat suma
            }
            if ("scadere".equals(choice)) {
                polinom = model.scadere();
            }
            if ("inmultire".equals(choice)) {
                polinom = model.inmultire();
            }
            if("derivare".equals((choice))) {
                polinom = model.derivare();
            }
            if("integrare".equals((choice))) {
                polinom = model.integrare();
            }
            if("impartire".equals((choice))) {
                polinom = model.impartire();
                rest = model.getRest(); // mai adaug restul
            }
            if("impartire".equals((choice)))
                view.setRezultat("CAT:" +polinom.getTermeni() + "  REST:"+ rest.getTermeni()); //la impartire pun si rest
            else view.setRezultat(polinom.getTermeni()); // daca nu, il pun simplu. Setez la output
        }
    }

}
